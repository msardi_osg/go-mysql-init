package sqldb

import (
	"database/sql"
	"os"
	"reflect"

	_ "github.com/go-sql-driver/mysql"
)

// DB is a global variable to hold db connection
var DB *sql.DB

// ConnectDB opens a connection to the database
func ConnectDB() {
	DB_IP := os.Getenv("DB_IP")
	DB_NAME := os.Getenv("DB_NAME")
	DB_USER := os.Getenv("DB_USER")
	DB_PASS := os.Getenv("DB_PASS")

	connStr := DB_USER + ":" + DB_PASS + "@tcp(" + DB_IP + ")/" + DB_NAME

	db, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}

	DB = db
}

// NullString is an alias for sql.NullString data type
type NullString sql.NullString

// Scan implements the Scanner interface for NullString
func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{s.String, false}
	} else {
		*ns = NullString{s.String, true}
	}

	return nil
}
